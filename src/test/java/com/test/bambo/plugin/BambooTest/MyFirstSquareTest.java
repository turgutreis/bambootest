package com.test.bambo.plugin.BambooTest;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyFirstTaskTest {

    @Test
    public void square() {

        MyFirstTask test = new MyFirstTask();
        int output = test.square(5);
        assertEquals(25, output);

    }
}